FROM maven:3.8.2-openjdk-17


COPY . /app

WORKDIR /app


RUN mvn package
