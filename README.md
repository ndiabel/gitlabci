# Projet GitLab CI/CD

Ce référentiel contient la configuration GitLab CI/CD pour un projet Java utilisant Maven et Docker. La pipeline est conçue pour construire, tester, empaqueter et déployer le projet.

## Prérequis

- GitLab CI/CD Runner configuré
- Docker installé
- Maven installé

## Structure de la Pipeline

La pipeline se compose de quatre étapes :

1. **Construction :** Compile le projet Maven.
2. **Test :** Exécute les tests pour le projet Maven.
3. **Empaquetage :** Emballe le projet Maven dans un fichier JAR.
4. **Déploiement :** Construit une image Docker, la pousse dans un registre Docker et déploie à l'aide de Docker Compose.

## Détails de la Configuration

### Image

La pipeline utilise l'image Maven avec OpenJDK 17.

```yaml
image: maven:3.8-openjdk-17
```

### Étapes

Définit les étapes de la pipeline.

```yaml
stages:
  - build
  - test
  - package
  - deploy
```

### Cache

Met en cache les dépendances Maven pour accélérer les exécutions de pipeline ultérieures.

```yaml
cache:
  paths:
    - .m2/repository
```

### Avant le Script

Définit les variables d'environnement pour Maven.

```yaml
before_script:
  - export MAVEN_OPTS=$MAVEN_OPTS
```

### Services

Spécifie Docker en tant que service à utiliser dans la pipeline.

```yaml
services:
  - docker:latest
```

### Jobs

#### Job de Construction

Compile le projet Maven.

```yaml
build_job:
  stage: build
  script:
    - echo "Compilation Maven démarrée"
    - mvn compile
```

#### Job de Test

Exécute les tests pour le projet Maven.

```yaml
test_job:
  stage: test
  script:
    - echo "Tests Maven démarrés"
    - mvn test
```

#### Job d'Empaquetage

Emballe le projet Maven dans un fichier JAR.

```yaml
package_job:
  stage: package
  script:
    - echo "Empaquetage Maven démarré"
    - mvn package
  artifacts:
    paths:
      - target/*.jar
```

#### Job de Déploiement

Construit une image Docker, la pousse dans un registre Docker et déploie à l'aide de Docker Compose.

```yaml
deploy_job:
  stage: deploy
  image: docker:latest
  script:
    - echo "Construction de l'image Docker"
    - docker build -t diabel/gitlab-ci:latest .
    - docker login -u diabel -p Pape12345
    - docker push diabel/gitlab-ci:latest
    - docker-compose -f docker-compose.production.yml up -d
  dependencies:
    - package_job
```

## Utilisation

Pour utiliser cette configuration GitLab CI/CD, incluez simplement le fichier `.gitlab-ci.yml` dans votre référentiel GitLab et assurez-vous que les prérequis sont satisfaits. La pipeline sera automatiquement déclenchée à chaque push sur le référentiel.

## Contribution

Si vous rencontrez des problèmes avec la pipeline ou avez des suggestions d'amélioration, n'hésitez pas à ouvrir une issue ou à soumettre une pull request.

---
Auteur: Pape Diabel SECK(diabel1208@gmail.com)
